package com.antonov.moodlerecommender.scheduler;

import com.antonov.moodlerecommender.util.HdfsUtil;
import org.springframework.scheduling.annotation.Scheduled;

public class Scheduler {

    @Scheduled(cron = "0 0 * * * *")
    public void loadMySQLData() throws Exception {
        HdfsUtil.loadMySQLData();
    }

    @Scheduled(cron = "0 0/30 * * * *")
    public void runMahoutJob() throws Exception {
        HdfsUtil.runMahoutJob();
    }
}
