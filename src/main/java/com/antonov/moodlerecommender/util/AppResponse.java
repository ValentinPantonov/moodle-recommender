package com.antonov.moodlerecommender.util;

import io.netty.handler.codec.http.HttpResponseStatus;
import org.apache.http.HttpResponse;

import java.io.Serializable;

public class AppResponse implements Serializable {

    private int responseCode;

    private String responseDescription;

    private Object data;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDescription() {
        return responseDescription;
    }

    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public AppResponse() {

    }

    public AppResponse(int code, String description) {
        this.responseCode = code;
        this.responseDescription = description;
        this.data = "";
    }

    public AppResponse(int code, Exception e) {
        this.responseCode = code;
        this.responseDescription = e.getMessage();
        this.data = "";
    }

    public AppResponse(int code, String description, Exception e) {
        this.responseCode = code;
        this.responseDescription = description;
        this.data = "";
    }

    public AppResponse(Object data) {
        this.responseCode = HttpResponseStatus.OK.code();
        this.responseDescription = HttpResponseStatus.OK.reasonPhrase();
        this.data = data;
    }

    public AppResponse(Exception e) {
        this.responseCode = HttpResponseStatus.INTERNAL_SERVER_ERROR.code();
        this.responseDescription = e.getMessage();
        this.data = "";
    }

}
