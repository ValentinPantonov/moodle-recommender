package com.antonov.moodlerecommender.util;

import com.antonov.moodlerecommender.mysql.DBInputWritable;
import com.antonov.moodlerecommender.mysql.DBMapper;
import com.antonov.moodlerecommender.mahout.MahoutJobMapper;
import com.antonov.moodlerecommender.mahout.MahoutJobReducer;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.ObjectWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.db.DBConfiguration;
import org.apache.hadoop.mapreduce.lib.db.DBInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.apache.hadoop.fs.Path;

import javax.annotation.PostConstruct;
import java.net.URI;


@Component
public class HdfsUtil {
    @Value("${hdfs.path}")
    private String hdfsPath;
    @Value("${hdfs.username}")
    private String hdfsUserName;
    @Value("${hdfs.mysql.data.path}")
    private String hdfsMysqlDataPath;
    @Value("${hdfs.mahout.data.path}")
    private String hdfsMahoutDataPath;
    @Value("${hdfs.data.default.file.name}")
    private String hdfsDataDefaultFileName;

    private static String staticHdfsPath;
    private static String staticHdfsUserName;
    private static String staticHdfsMysqlDataPath;
    private static String staticHdfsMahoutDataPath;
    private static String staticHdfsDataDefaultFileName;

    private static final String jobCompleted = "Job Completed";
    private static final String jobFailed = "Job Failed";

    private static Configuration getConfiguration() {
        Configuration configuration = new Configuration();
        configuration.set("fs.defaultFS", staticHdfsPath);
        return configuration;
    }

    public static FileSystem getFileSystem() throws Exception {
        return FileSystem.get(new URI(staticHdfsPath), getConfiguration(), staticHdfsUserName);
    }

    public static boolean existFile(String path) throws Exception {
        if (StringUtils.isEmpty(path)) {
            return false;
        }
        FileSystem fs = getFileSystem();
        Path srcPath = new Path(path);
        return fs.exists(srcPath);
    }

    public static String loadMySQLData() throws Exception {
        Configuration conf = getConfiguration();
        DBConfiguration.configureDB(conf, "com.mysql.jdbc.Driver", // driver class
                "jdbc:mysql://localhost:3306/moodle", // db url
                "****", // user name
                "****"); // password
        Job job = Job.getInstance(conf);
        job.setMapperClass(DBMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(NullWritable.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(NullWritable.class);
        job.setInputFormatClass(DBInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        DBInputFormat.setInput(job, DBInputWritable.class, "mdl_user_enrolments", // table name
                null, null,
                "enrolid", "userid"); // table columns

        FileOutputFormat.setOutputPath(job, new Path(staticHdfsMysqlDataPath));
        if (existFile(staticHdfsMysqlDataPath)) {
            getFileSystem().delete(new Path(staticHdfsMysqlDataPath), true);
        }
        if (job.waitForCompletion(true)) {
            return jobCompleted;
        } else {
            return jobFailed;
        }
    }

    public static String runMahoutJob() throws Exception {
        Configuration configuration = getConfiguration();
        Job job = Job.getInstance(configuration, "MahoutJob");
        job.setMapperClass(MahoutJobMapper.class);
        job.setReducerClass(MahoutJobReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(ObjectWritable.class);
        FileInputFormat.setInputPaths(job, new Path(staticHdfsMysqlDataPath + staticHdfsDataDefaultFileName));
        FileOutputFormat.setOutputPath(job, new Path(staticHdfsMahoutDataPath));
        if (existFile(staticHdfsMahoutDataPath)) {
            getFileSystem().delete(new Path(staticHdfsMahoutDataPath), true);
        }
        if (job.waitForCompletion(true)) {
            return jobCompleted;
        } else {
            return jobFailed;
        }
    }


    @PostConstruct
    public void hdfsPath() {
        staticHdfsPath = this.hdfsPath;
    }

    @PostConstruct
    public void hdfsUserName() {
        staticHdfsUserName = this.hdfsUserName;
    }

    @PostConstruct
    public void hdfsMysqlDataPath() {
        staticHdfsMysqlDataPath = this.hdfsMysqlDataPath;
    }

    @PostConstruct
    public void hdfsMahoutDataPath() {
        staticHdfsMahoutDataPath = this.hdfsMahoutDataPath;
    }

    @PostConstruct
    public void hdfsDataDefaultFileName() {
        staticHdfsDataDefaultFileName = this.hdfsDataDefaultFileName;
    }

}
