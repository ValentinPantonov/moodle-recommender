package com.antonov.moodlerecommender.mysql;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class DBMapper extends Mapper<LongWritable, DBInputWritable, Text, NullWritable> {
    protected void map(LongWritable id, DBInputWritable value, Context ctx) {
        try {
            String pageDetails = value.getUserid() + "," + value.getEnrolid();
            ctx.write(new Text(pageDetails), NullWritable.get());
        } catch (IOException | InterruptedException ioException) {
            ioException.printStackTrace();
        }
    }
}
