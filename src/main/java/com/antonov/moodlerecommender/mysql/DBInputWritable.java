package com.antonov.moodlerecommender.mysql;


import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.lib.db.DBWritable;

/*
   CREATE TABLE `mdl_user_enrolments` (
    `id` bigint(10) NOT NULL AUTO_INCREMENT,
    `status` bigint(10) NOT NULL DEFAULT 0,
    `enrolid` bigint(10) NOT NULL,
    `userid` bigint(10) NOT NULL,
    `timestart` bigint(10) NOT NULL DEFAULT 0,
    `timeend` bigint(10) NOT NULL DEFAULT 2147483647,
    `modifierid` bigint(10) NOT NULL DEFAULT 0,
    `timecreated` bigint(10) NOT NULL DEFAULT 0,
    `timemodified` bigint(10) NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    UNIQUE KEY `mdl_userenro_enruse_uix` (`enrolid`,`userid`),
    KEY `mdl_userenro_enr_ix` (`enrolid`),
    KEY `mdl_userenro_use_ix` (`userid`),
    KEY `mdl_userenro_mod_ix` (`modifierid`)
   )
*/
public class DBInputWritable implements Writable, DBWritable {
    private long enrolid;

    private long userid;

    public void write(PreparedStatement statement) throws SQLException {
        statement.setLong(1, enrolid);
        statement.setLong(2, userid);

    }

    public void readFields(ResultSet resultSet) throws SQLException {
        enrolid = resultSet.getInt(1);
        userid = resultSet.getLong(2);
    }

    public void write(DataOutput out) throws IOException {
    }

    public void readFields(DataInput in) throws IOException {
    }

    public long getEnrolid() {
        return enrolid;
    }

    public long getUserid() {
        return userid;
    }
}
