package com.antonov.moodlerecommender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoodleRecommenderApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoodleRecommenderApplication.class, args);
    }

}
