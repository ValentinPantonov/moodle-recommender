package com.antonov.moodlerecommender.model;

import javax.persistence.Entity;

@Entity
public class Course {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
