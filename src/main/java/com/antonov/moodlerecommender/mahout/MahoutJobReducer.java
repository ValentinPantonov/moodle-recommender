package com.antonov.moodlerecommender.mahout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.ObjectWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.common.FastByIDMap;
import org.apache.mahout.cf.taste.impl.common.LongPrimitiveIterator;
import org.apache.mahout.cf.taste.impl.model.GenericDataModel;
import org.apache.mahout.cf.taste.impl.model.GenericUserPreferenceArray;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.LogLikelihoodSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.model.PreferenceArray;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;


public class MahoutJobReducer extends Reducer<Text, ObjectWritable, Text, NullWritable> {
    public void reduce(Text key, Iterable<ObjectWritable> values, Context context) throws
            IOException, InterruptedException {

        FastByIDMap<PreferenceArray> preferences = new FastByIDMap<>();

        Map<Long, List<Long>> prefMap = new HashMap<>();
        for (ObjectWritable value : values) {
            String toString = (String) value.get();
            long enrolId = Long.parseLong(toString.split(",")[1], 10);
            long userId = Long.parseLong(toString.split(",")[0], 10);
            List<Long> userPrefs;
            if (!prefMap.containsKey(userId)) {
                userPrefs = new ArrayList<>();
            } else {
                userPrefs = prefMap.get(userId);
            }
            userPrefs.add(enrolId);
            prefMap.put(userId, userPrefs);
        }

        for (Long userId : prefMap.keySet()) {
            List<Long> prefsList = prefMap.get(userId);
            PreferenceArray prefsForUser = new GenericUserPreferenceArray(prefsList.size());
            int prefIdx = 0;
            prefsForUser.setUserID(0, userId);
            for (Long enrolId : prefsList) {
                prefsForUser.setItemID(prefIdx, enrolId);
                prefsForUser.setValue(prefIdx, 1.0F);
                prefIdx++;
            }
            preferences.put(userId, prefsForUser);
        }

        DataModel dataModel = new GenericDataModel(preferences);
        ItemSimilarity similarity = new LogLikelihoodSimilarity(dataModel);
        GenericItemBasedRecommender recommender = new GenericItemBasedRecommender(dataModel, similarity);

        Map<TwoItemKey, Float> itemToItemSimilarities = new HashMap<>();
        try {
            for (LongPrimitiveIterator items = dataModel.getItemIDs(); items.hasNext(); ) {

                long itemId = items.nextLong();
                List<RecommendedItem> recommendations = recommender.mostSimilarItems(itemId, 100);

                for (RecommendedItem recommendation : recommendations) {

                    if (!itemToItemSimilarities.containsKey(new TwoItemKey(itemId, recommendation.getItemID()))) {
                        itemToItemSimilarities.put(new TwoItemKey(itemId, recommendation.getItemID()), recommendation.getValue());
                    }
                }
            }

            StringBuilder output = new StringBuilder();
            for (TwoItemKey twoItemKey : itemToItemSimilarities.keySet()) {
                output.append("Enrolments: ")
                        .append(twoItemKey.getVal1())
                        .append(", ")
                        .append(twoItemKey.getVal2())
                        .append(" Similarity: ")
                        .append(itemToItemSimilarities.get(twoItemKey))
                        .append("\n");
            }
            context.write(new Text(output.toString()), NullWritable.get());
        } catch (TasteException e) {
            context.write(new Text(e.getMessage()), NullWritable.get());
        }

    }

    public static class TwoItemKey {
        private long val1;
        private long val2;

        public TwoItemKey(long val1, long val2) {
            this.val1 = val1;
            this.val2 = val2;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TwoItemKey myKey = (TwoItemKey) o;
            return val1 == myKey.val1 || val1 == myKey.val2 || val2 == myKey.val2;
        }

        @Override
        public int hashCode() {
            return new Long(this.val1 + this.val2).hashCode();
        }

        public long getVal1() {
            return val1;
        }

        public void setVal1(long val1) {
            this.val1 = val1;
        }

        public long getVal2() {
            return val2;
        }

        public void setVal2(long val2) {
            this.val2 = val2;
        }

    }

}
