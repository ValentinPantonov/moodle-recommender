package com.antonov.moodlerecommender.mahout;

import java.io.IOException;

import org.apache.hadoop.io.ObjectWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MahoutJobMapper extends Mapper<Object, Text, Text, ObjectWritable> {

    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        context.write(new Text("content"), new ObjectWritable(value.toString()));
    }

}
