package com.antonov.moodlerecommender.controller;

import com.antonov.moodlerecommender.service.UserService;
import com.antonov.moodlerecommender.util.AppResponse;
import com.antonov.moodlerecommender.util.HdfsUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/hadoop/hdfs")
public class HdfsController {

    private final UserService userService;

    @Autowired
    public HdfsController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/mysql-data")
    public AppResponse loadMySQLData() {
        try {
            return new AppResponse(HdfsUtil.loadMySQLData());
        } catch (Exception e) {
            return new AppResponse(e);
        }
    }

    @PostMapping("/mahout-job")
    public AppResponse runMahoutJob() {
        try {
            return new AppResponse(HdfsUtil.runMahoutJob());
        } catch (Throwable e ) {
            return new AppResponse(e);
        }
    }

    @GetMapping("/user")
    public AppResponse getUserRecommendations(@RequestParam String username) {
        try {
            return new AppResponse(userService.getUserRecommendations(username));
        } catch (Throwable e ) {
            return new AppResponse(e);
        }
    }
}


